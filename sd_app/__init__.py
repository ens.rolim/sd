#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask

def create_app(config=None):
    app = Flask(__name__, instance_relative_config=False)
    
    app.logger.info("Carregando configurações do arquivo {0}.".format(config))
    app.config.from_pyfile(config)
    
    from sd_app.blueprints import main
    from sd_app.blueprints import usuarios
    
    app.logger.info("Registrando blueprints.")
    app.register_blueprint(main, url_prefix='/')
    app.register_blueprint(usuarios, url_prefix='/usuarios')
    
    app.logger.info("Iniciando objetos compartilhados do projeto.")
    from sd_app.compartilhado import sqlalchemy as banco, migrate
    banco.init_app(app)
    migrate.init_app(app, banco)
    
    return app
